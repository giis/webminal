import React, { memo, useEffect, useState } from "react";
import DocSidebar from "@theme-original/DocSidebar";
import SectionChanger from "../../../components/SectionChanger";
import Logo from "@theme/Logo";
import CollapseButton from "@theme/DocSidebar/Desktop/CollapseButton";
import Content from "@theme/DocSidebar/Desktop/Content";
import { useThemeConfig } from "@docusaurus/theme-common";

export default function DocSidebarDesktop({
  path,
  sidebar,
  onCollapse,
  isHidden,
}) {
  const {
    navbar: { hideOnScroll },
    docs: {
      sidebar: { hideable },
    },
  } = useThemeConfig();
  return (
    <>
      <div
        className={[
          hideOnScroll && styles.sidebarWithHideableNavbar,
          isHidden && styles.sidebarHidden,
        ].join(" ")}
      >
        {hideOnScroll && <Logo tabIndex={-1} className={styles.sidebarLogo} />}
        <SectionChanger />
        <Content path={path} sidebar={sidebar} />
        {hideable && <CollapseButton onClick={onCollapse} />}
      </div>
    </>
  );
}
