import BrowserOnly from "@docusaurus/BrowserOnly";
import React, { useEffect, useState } from "react";
import Terminal from "../components/Terminal";
import AuthForm from "../components/AuthForm/AuthForm";
import { supabase } from "../supabase/index";
import { useHistory } from "@docusaurus/router";
import { useLocation } from "@docusaurus/router";
export default function Root({ children }) {
  const [user, setUser] = useState(null);
  const history = useHistory();
  const location = useLocation();
  useEffect(() => {
    const session = supabase.auth.session();
    setUser(session?.user ?? null);
    const { data: authListener } = supabase.auth.onAuthStateChange(
      async (event, session) => {
        const currentUser = session?.user;

        setUser(currentUser ?? null);
      }
    );

    return () => {
      authListener?.unsubscribe();
    };
  }, [user]);

  useEffect(() => {
    const checkIfFirstTime = async () => {
      return await supabase
        .from("profiles")
        .select("id,  avatarurl")
        .filter("id", "eq", user.id);
    };
    if (user) {
      document.body.style.setProperty("--is-auth", "none");
      checkIfFirstTime().then((res) => {
        console.log(res);
        //so it is alraedy in db
        if (res.data.length === 0) {
          history.push("/welcome");
        }
      });
    }
  }, [user, location.pathname]);

  return (
    <>
      {children}

      <BrowserOnly>{() => <Terminal />}</BrowserOnly>
    </>
  );
}
