import React, { useEffect, useState } from "react";
import Content from "@theme-original/Navbar/Content";
import { supabase } from "../../../supabase";
import SectionChanger from "../../../components/SectionChanger";

export default function ContentWrapper(props) {
  const [user, setUser] = useState(null);

  useEffect(() => {
    const session = supabase.auth.session();
    setUser(session?.user ?? null);
    const { data: authListener } = supabase.auth.onAuthStateChange(
      async (event, session) => {
        const currentUser = session?.user;

        setUser(currentUser ?? null);
      }
    );

    return () => {
      authListener?.unsubscribe();
    };
  }, [user]);

  const onLogOut = () => {
    supabase.auth.signOut();
    document.body.style.setProperty("--is-auth", "initial");
  };
  console.log("Navbar", props);
  return (
    <>
      <Content {...props} />
      {user && (
        <button
          onClick={onLogOut}
          style={{ height: "100%", textAlign: "center" }}
        >
          Logout
        </button>
      )}
    </>
  );
}
