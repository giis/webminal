import React from "react";
//@ts-ignore
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
//@ts-ignore
import Layout from "@theme/Layout";
import { VscTerminalLinux } from "react-icons/vsc";
import GlowingBox from "../components/GlowingBox";
import DropDown from "../components/DropDown";

const Frame = () => (
  <div className="w-full h-full">
    <textarea
      className="w-full h-full bg-neutral-900 text-green-400 text-center"
      defaultValue={"  Cool Terminal"}
    ></textarea>
  </div>
);

export default function Home(): JSX.Element {
  const { siteConfig } = useDocusaurusContext();
  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Description will go into a meta tag in <head />"
    >
      <main>
        <br />
        <br />
        <br />
        <br />
        <div className="container max-w-screen-lg ">
          <div className="w-100 justify-center mt-5 h-auto  flex flex-col md:flex-row">
            <div className="flex flex-col grow  justify-center content-center text-center sm:w-full md:w-3/6 p-5">
              <h1
                className="text-7xl font-bold "
                style={{
                  background:
                    "linear-gradient(to right, var(--ifm-color-secondary-darker),var(--ifm-color-primary-darkest))",
                  WebkitTextFillColor: "transparent",
                  WebkitBackgroundClip: "text",
                }}
              >
                Write videos <br />
                in React.
              </h1>
              <p className=" text-left">
                Use your React knowledge to create real MP4 videos. Scale your
                video production using server-side rendering and
                parametrization.
              </p>
            </div>
            <div className="grow sm:w-full md:w-3/6 hover:scale-125 transition-transform">
              <GlowingBox>
                <Frame />
              </GlowingBox>
            </div>
          </div>
          <br />
          <br />
          <div className=" mt-6 rounded-md">
            <h2 className="text-center text-4xl p-3">Some text</h2>
            <div
              className="  content-center p-2 grid justify-between mb-5 "
              style={{
                gridTemplateColumns: "repeat(auto-fill, 100px)",
              }}
            >
              <div className="flex flex-col items-center">
                <VscTerminalLinux size={"50px"} />
                <div className="w-full text-center">Linux</div>
              </div>
              <div className="flex flex-col items-center">
                <VscTerminalLinux size={"50px"} />
                <div className="w-full text-center">Linux</div>
              </div>
              <div className="flex flex-col items-center">
                <VscTerminalLinux size={"50px"} />
                <div className="w-full text-center">Linux</div>
              </div>{" "}
              <div className="flex flex-col items-center">
                <VscTerminalLinux size={"50px"} />
                <div className="w-full text-center">Linux</div>
              </div>
              <div className="flex flex-col items-center">
                <VscTerminalLinux size={"50px"} />
                <div className="w-full text-center">Linux</div>
              </div>{" "}
              <div className="flex flex-col items-center">
                <VscTerminalLinux size={"50px"} />
                <div className="w-full text-center">Linux</div>
              </div>
              <div className="flex flex-col items-center">
                <VscTerminalLinux size={"50px"} />
                <div className="w-full text-center">Linux</div>
              </div>{" "}
              <div className="flex flex-col items-center">
                <VscTerminalLinux size={"50px"} />
                <div className="w-full text-center">Linux</div>
              </div>
              <div className="flex flex-col items-center">
                <VscTerminalLinux size={"50px"} />
                <div className="w-full text-center">Linux</div>
              </div>
            </div>
          </div>
          <br />
          <br />
          <div className="container max-w-screen-lg mb-5">
            <div className="w-100 justify-center mt-5 h-auto  flex flex-col md:flex-row">
              <div className="grow sm:w-full md:w-3/6 hover:scale-125 transition-transform">
                <GlowingBox>
                  <Frame />
                </GlowingBox>
              </div>
              <div className="flex flex-col grow  justify-center content-center text-center sm:w-full md:w-3/6 p-5">
                <h1
                  className="text-7xl font-bold "
                  style={{
                    background:
                      "linear-gradient(to right, var(--ifm-color-primary-darkest),var(--ifm-color-secondary-darker))",
                    WebkitTextFillColor: "transparent",
                    WebkitBackgroundClip: "text",
                  }}
                >
                  Write videos <br />
                  in React.
                </h1>
                <p className=" text-left">
                  Use your React knowledge to create real MP4 videos. Scale your
                  video production using server-side rendering and
                  parametrization.
                </p>
              </div>
            </div>
          </div>
        </div>
        <br />
        <br />
        <div className="w-1/2 m-auto mb-3">
          <DropDown>
            <div className="border-white border-2 w-fit p-2">React</div>
            <div className="border-white border-2 w-fit p-2">Angular</div>
            <div className="border-white border-2 w-fit p-2">Vue</div>
          </DropDown>
        </div>
      </main>
    </Layout>
  );
}
