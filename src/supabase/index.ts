//@ts-ignore
import siteConfig from "@generated/docusaurus.config";
import { createClient } from "@supabase/supabase-js";

console.log(siteConfig);

const REACT_APP_SUPABASE_URL = siteConfig.customFields.SUPABASE_URL;
const REACT_APP_SUPABASE_KEY = siteConfig.customFields.SUPABASE_KEY;

export const supabase = createClient(
  REACT_APP_SUPABASE_URL,
  REACT_APP_SUPABASE_KEY
);
