import React, { useEffect, useLayoutEffect, useRef } from "react";
import { ReactNode, useState } from "react";
import { AiOutlineArrowDown, AiOutlineCheck } from "react-icons/ai";

type DropDownProps = {
  children: ReactNode[];
  className?: string;
  onChange?: (index: number) => void;
  style: any;
  defaultIndex?: number;
};

export default function DropDown({
  children,
  className = "",
  onChange = () => {},
  style,
  defaultIndex = 0,
}: DropDownProps) {
  const [isChooseMode, setIsChooseMode] = useState(false);
  const [activeIndex, setActiveIndex] = useState(defaultIndex);
  const [offsetTop, setOffsetTop] = useState("0px");
  const liRef = useRef();

  useLayoutEffect(() => {
    if (!isChooseMode) return;

    const LiS = (liRef.current as any).children;
    setOffsetTop(-LiS[activeIndex].offsetTop + "px");
  }, [isChooseMode, activeIndex]);

  if (!isChooseMode)
    return (
      <div
        className={className + " flex items-center"}
        onClick={() => setIsChooseMode(true)}
        style={style}
      >
        {children.filter((_, ind) => ind === activeIndex)}
        <AiOutlineArrowDown className="ml-2" />
      </div>
    );

  return (
    <div style={style} className={className + " relative "}>
      <ul
        className="relative"
        ref={liRef as any}
        style={{ top: offsetTop } as any}
      >
        {children.map((el, ind) => (
          <li
            key={ind}
            onClick={() => {
              setActiveIndex(ind);
              setIsChooseMode(false);
              onChange(ind);
            }}
            className="relative flex my-2 items-center"
            // style={{ top: "var(--offset-top)" }}
          >
            {el}
            {activeIndex === ind && <AiOutlineCheck className="ml-2" />}
          </li>
        ))}
      </ul>
    </div>
  );
}
