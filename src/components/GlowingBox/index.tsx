import { ReactNode } from "react";
import React from "react";
import styles from "./index.module.css";
const GlowingBox = ({ children }: { children: ReactNode }) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles["glowing-div"]}></div>
      <div className={styles.container}>{children}</div>
    </div>
  );
};

export default GlowingBox;
