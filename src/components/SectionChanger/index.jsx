import DropDown from "../../components/DropDown";
import React, { memo, useEffect, useState } from "react";
import { useHistory } from "@docusaurus/router";

const Regex = /\/(react|vue|angular)(\/)?/;
const SectionChanger = ({ className = "" }) => {
  const history = useHistory();

  const [currentSection, setCurrentSection] = useState(() => {
    if (/react(\/)?/.test(history.location.pathname)) return 0;

    if (/angular(\/)?/.test(history.location.pathname)) return 1;
    if (/vue(\/)?/.test(history.location.pathname)) return 2;
    console.log("DEfault");
    return 0;
  });

  useEffect(() => {
    console.log("CHANGED", history.location.pathname);
    if (/react(\/)?/.test(history.location.pathname) && currentSection !== 0)
      return setCurrentSection(0);

    if (/angular(\/)?/.test(history.location.pathname) && currentSection !== 1)
      setCurrentSection(1);
    if (/vue(\/)?/.test(history.location.pathname) && currentSection !== 2)
      setCurrentSection(2);
  }, [history.location.pathname]);

  useEffect(() => {
    let ind = currentSection;
    switch (ind) {
      case 0:
        history.push(history.location.pathname.replace(Regex, "") + "/react/");

        break;
      case 2:
        history.push(history.location.pathname.replace(Regex, "") + "/vue/");

        break;
      case 1:
        history.push(
          history.location.pathname.replace(Regex, "") + "/angular/"
        );

        break;
    }
  }, [currentSection]);

  return (
    <DropDown
      key={currentSection}
      className={"relative p-3 flex column align-center " + className}
      style={{
        marginTop: "var(--ifm-navbar-height)",
        paddingTop: "var(--ifm-navbar-height)",
      }}
      onChange={(ind) => setCurrentSection(ind)}
      defaultIndex={currentSection}
    >
      <span className="p-2 border-2 border-green"> React </span>
      <span className="p-2 border-2 border-green"> Angular </span>
      <span className="p-2 border-2 border-green"> Vue </span>
    </DropDown>
  );
};

export default SectionChanger;
