import React, { useState } from "react";
import { VscGithub } from "react-icons/vsc";
import { supabase } from "../../supabase";
import Spinner from "../Spinner";
//@ts-ignore
import styles from "./form.module.css";

import { MdEmail } from "react-icons/md";

const AuthForm = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [email, setEmail] = useState("");
  const onEmailSubmit = () => {
    setIsLoading(true);
    supabase.auth
      .signIn({ email })
      .then((res) => {
        console.log(res);
        alert("Check your email please");
      })
      .catch((err) => {
        alert("An error occured. Please try again");

        console.log(err);
      })
      .finally(() => setIsLoading(false));
  };

  const onGithubSubmit = () => {
    setIsLoading(true);
    supabase.auth
      .signIn({ provider: "github" }, { redirectTo: "./welcome" })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        alert("An error occured. Please try again");

        console.log(err);
      })
      .finally(() => setIsLoading(false));
  };

  if (isLoading)
    return (
      <div className="h-screen w-[99%] flex justify-center items-center overflow-hidden">
        <div className="w-[77vmin] aspect-square ">
          <Spinner />
        </div>
      </div>
    );

  return (
    <div>
      <div className="flex items-center justify-center h-screen w-full">
        <div className="sm:w-full md:w-3/6 h-screen m-5 p-1 bg-slate-700 border-slate-900 rounded-lg text-stone-900 ">
          <h1 className="text-center text-5xl p-2 mt-5">
            Please sign-up/sign-in!
          </h1>
          <div className="flex justify-center flex-col h-full">
            <div className="flex md:flex-row flex-col gap-1.5 items-center ">
              <div className="flex-1 flex flex-col items-center justify-between h-full bg-slate-200 p-5">
                <div>
                  <VscGithub size={"50px"} />
                  <span>Github</span>
                </div>

                <button
                  onClick={onGithubSubmit}
                  className="bg-green-700 p-5 rounded-full w-40"
                >
                  Login
                </button>
              </div>

              <div className="flex-1 flex flex-col items-center gap-1.5 justify-between h-full bg-slate-200 p-5">
                <div>
                  <MdEmail size={"50px"} />

                  <span> Magic link</span>
                </div>

                <input
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  type="email"
                  placeholder="Your email"
                  className="w-40"
                />
                <button
                  onClick={onEmailSubmit}
                  className="bg-green-700 p-5 rounded-full w-40"
                >
                  Submit
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AuthForm;
