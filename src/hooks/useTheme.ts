import { useEffect, useState } from "react";

export const useTheme = () => {
  const [theme, setTheme] = useState(
    (document.querySelector("html") as any).getAttribute("data-theme")
  );
  useEffect(() => {
    var observer = new MutationObserver(function (mutations) {
      mutations.forEach(function (mutation) {
        if (mutation.type === "attributes") {
          setTheme(
            (document.querySelector("html") as any).getAttribute("data-theme")
          );
        }
      });
    });

    observer.observe(document.querySelector("html") as any, {
      attributes: true, //configure it to listen to attribute changes
    });
  }, []);
  return theme;
};
